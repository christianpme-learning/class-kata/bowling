package bowling;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class FrameTest{

    @Test
    public void frameInitTest(){
        Frame f = new Frame();
        
        assertTrue(f.getPinsRolled().length==0);
        assertTrue(f.getScore()==0);
    }

    @Test
    public void setPinsRolledTest(){
        Frame f = new Frame();

        int[] pinsRolled=new int[2];
        pinsRolled[0] = 1;
        f.setPinsRolled(pinsRolled);

        assertTrue(f.getPinsRolled()[0]==pinsRolled[0]);
    }

    @Test
    public void getScoreTest(){
        Frame f = new Frame();
        int[] pinsRolled=new int[2];
        pinsRolled[0] = 1;
        pinsRolled[1] = 4;
        f.setPinsRolled(pinsRolled);

        assertTrue(f.getScore()==5);
    }
}