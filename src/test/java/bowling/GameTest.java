package bowling;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class GameTest{

    @Test
    public void gamePlayTest(){
        Game game = new Game();
        int[] rolls = new int[]{1,4,4,5,6,4,5,5,10,0,1,7,3,6,4,10,2,8,6};
        for(int pins : rolls){
            game.addRoll(pins);
        }
        
        assertTrue(game.getTotalScore()==133);
    }

    @Test
    public void gameInitTest(){
        Game game = new Game();

        assertTrue(game.getTotalScore()==0);
        assertTrue(game.getFrames().size()==10);
        assertTrue(game.isGameOver()==false);
    }

    @Test
    public void addFirstRollTest(){
        Game game = new Game();

        game.addRoll(1);

        assertTrue(game.getTotalScore()==1);
    }

    @Test
    public void addSecondRollTest(){
        Game game = new Game();

        game.addRoll(1);
        game.addRoll(4);

        assertTrue(game.getTotalScore() == 5);
        assertTrue(game.getFrames().size() == 10);
    }

    @Test
    public void totalScoreMultipleFramesTest(){
        Game game = new Game();

        game.addRoll(1);
        game.addRoll(4);
        game.addRoll(4);
        game.addRoll(0);

        assertTrue(game.getTotalScore()==9);
    }

    @Test
    public void totalScoreStrikeTest(){
        Game game = new Game();

        game.addRoll(10);
        game.addRoll(1);
        game.addRoll(1);

        //10 + (1) + (1) + 1 + 1
        assertTrue(game.getTotalScore()==14);
    }

    @Test
    public void addRollZeroFrameTest(){
        Game game = new Game();

        game.addRoll(0);
        game.addRoll(0);

        assertTrue(game.getTotalScore()==0);
    }

    @Test
    public void addRollZeroFirstTest(){
        Game game = new Game();

        game.addRoll(0);
        game.addRoll(1);
        game.addRoll(7);

        assertTrue(game.getFrames().get(0).getScore()==1);
        assertTrue(game.getFrames().get(1).getScore()==7);
    }

    @Test
    public void addRollStrikeTest(){
        Game game = new Game();

        game.addRoll(10);
        game.addRoll(5);
        game.addRoll(4);


        assertTrue(game.getFrames().get(0).getPinsRolled().length==1);
        //10 + (5) + (4) + 5 + 4
        assertTrue(game.getTotalScore()==28);
    }

    @Test
    public void isGameOverTrueTest(){
        Game game = new Game();

        for(int frame=0; frame<10; ++frame){
            game.addRoll(1);
            game.addRoll(0);
        }

        assertTrue(game.isGameOver()==true);
    }

    @Test
    public void lastFrameAddRollSpareTest(){
        Game game = new Game();

        //first 9 frames
        for(int frame=0; frame<9; ++frame){
            game.addRoll(1);
            game.addRoll(0);
        }

        //last frame
        game.addRoll(9);
        game.addRoll(1); //spare
        game.addRoll(10); //strike

        //9 + 9 + 1 + 10
        assertTrue(game.getTotalScore()==29);
    }

    @Test
    public void lastFrameAddRollStrikeTest(){
        Game game = new Game();

        //first 9 frames
        for(int frame=0; frame<9; ++frame){
            game.addRoll(1);
            game.addRoll(0);
        }

        //last frame
        game.addRoll(10);
        game.addRoll(10); //strike

        //9 + 10 + 10
        assertTrue(game.getTotalScore()==29);
    }
}