package bowling;

import java.util.ArrayList;
import java.util.List;

public class Game {

	private List<Frame> frames = new ArrayList<>(10);
	private int frameIndex=0;

	public Game(){
		for(int i=0; i<10; ++i)
			frames.add(i, new Frame());
	}

	public int getTotalScore() {
		int totalScore=0;
		boolean spare=false;
		boolean strike=false;
		for(Frame f : frames){

			totalScore += nextFrameBonus(f, spare, strike);

			spare = f.isSpare();
			strike = f.isStrike();

			//actual frame
			totalScore += f.getScore();
		}
		return totalScore;
	}

	public int nextFrameBonus(Frame f, boolean spare, boolean strike){
		if(spare){
			return f.getPinsRolled()[0];
		}
		else if(strike){
			int buffer = f.getPinsRolled()[0];
			if(f.getPinsRolled().length>=2)
				buffer += f.getPinsRolled()[1];
			
			return buffer;
		}
		return 0;
	}

	public List<Frame> getFrames() {
		return frames;
	}

	public boolean isGameOver() {
		return frameIndex==9;
	}

	public void addRoll(int pins) {
		Frame f = frames.get(frameIndex);
		int[] pinsRolled;
		int tries=2;
		
		//strike
		if(isStrike(pins)){
			if(isLastFrame()){
				tries=2;
			}
			else{
				tries=1;
				++frameIndex;
			}
		}

		//spare
		if(f.isSpare() && isLastFrame()){
			tries=3;
			pinsRolled = new int[tries];
			for(int i=0; i<f.getPinsRolled().length; ++i){
				pinsRolled[i]=f.getPinsRolled()[i];
			}
		}
		else if(f.getPinsRolled().length==0){
			pinsRolled = new int[tries];
		}
		else{
			pinsRolled = f.getPinsRolled();
		}
		
		pinsRolled[actualTry(f)]=pins;
		f.setPinsRolled(pinsRolled);
	}

	public boolean isStrike(int pins){
		return pins==10;
	}

	public boolean isLastFrame(){
		return frameIndex==9;
	}

	public int actualTry(Frame f){
		if(f.isSpare() && isLastFrame()){
			return 2;
		}
		else if(f.getScore()==0 && f.getActualTry()==0){
			return 0;
		}
		else{
			if(!isLastFrame()){
				++frameIndex;
			}
			return 1;
		}
	}
}
