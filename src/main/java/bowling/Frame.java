package bowling;

public class Frame {

	private int[] pinsRolled = new int[0];
	private int score;

	private int actualTry=0;

	public int[] getPinsRolled() {
		return pinsRolled;
	}

	public int getScore() {
		return score;
	}

	public void setPinsRolled(int[] pinsRolled) {
		this.pinsRolled = pinsRolled;
		score=0;
		for(int pins : pinsRolled){
			score+=pins;
		}
		++actualTry;
	}

	public boolean isSpare(){
		return (pinsRolled.length==2 || pinsRolled.length==3) && pinsRolled[0]+pinsRolled[1] == 10;
	}

	public boolean isStrike(){
		return  (pinsRolled.length==1 || pinsRolled.length==2) && pinsRolled[0] == 10;
	}

	public int getActualTry() {
		return actualTry;
	}
}